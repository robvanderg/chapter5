#include "embeds.ih"

Embeds::Embeds(string const &vec, string const &cache, bool bin)
:
    d_cachePath(cache),
    d_rawW2V(vec)
{
    if (d_cachePath == "")
        d_cachePath = std::string(vec) + ".cache";
    if (bin)
        loadBin(d_cachePath);
    else
        loadTxt(d_cachePath);
}
