#include "embeds.ih"

double Embeds::getDistance(string const &word1, string const &word2)
{
    return d_rawW2V.distance(word1, word2);
}
