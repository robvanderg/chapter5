#include "main.ih"

void readConfig(Config *myConfig, std::vector<option::Option> options)
{//move to config class?
    //Fill paths
    std::string cleanPath(options[DIR].arg);
    if (cleanPath.back() != '/')
        cleanPath += '/';
    myConfig->w2v = cleanPath + "w2v.bin";
    myConfig->twitter = cleanPath + "twitter.ngr.bin";
    myConfig->wiki = cleanPath + "wiki.ngr.bin";
    myConfig->dict = cleanPath + "aspell";
    
    myConfig->aspLan = cleanPath.substr(cleanPath.find_last_of('/', cleanPath.length() -2) + 1, 2);
    myConfig->aspDict = "./" + cleanPath + "aspell-model";
 
    // binary
    myConfig->normAll = !options[UNK];
    myConfig->caps = options[CAPS];
    myConfig->goldErrDet = options[GOLD];
    myConfig->goldErrDet2 = (options[GOLD2] || options[ERRDET]);
    myConfig->sepErrDet = options[ERRDET];
    myConfig->tokenize = options[TOKENIZE];
    myConfig->verbose = options[VERBOSE];
    myConfig->treebankMode = options[SYNTACTIC];
    myConfig->wordPerLine = options[WORDLINE];

    // other
    myConfig->aspMode = options[BADSPELLER] ? "bad-spellers": "normal";
    myConfig->errDetWeight = options[ERRDET] ? std::stod(options[ERRDET].arg): 0.0;
    myConfig->numTrees = options[NUMTREES] ? std::stoi(options[NUMTREES].arg): 500;
    myConfig->onlyCanon = options[KNOWN] ? std::stoi(options[KNOWN].arg): 0;
    myConfig->dev = options[DEV] ? options[DEV].arg : "";
    myConfig->numTrain = options[NTRAIN] ? std::stoi(options[NTRAIN].arg): 0;
    myConfig->numThreads = options[THREADS] ? std::stoi(options[THREADS].arg) : 4;
    myConfig->numCands = options[CANDS]? std::stoi(options[CANDS].arg): 0;
    myConfig->weight = options[WEIGHT]? std::stod(options[WEIGHT].arg): 1.0;
    myConfig->seed = options[SEED]? std::stoi(options[SEED].arg): 5;
    myConfig->kfold = options[KFOLD]? std::stoi(options[KFOLD].arg): 10;
    myConfig->knowns = options[UNK]? options[UNK].arg: "";

    myConfig->regrPath = options[RANDOMF].arg;
    myConfig->cleanedPath = (myConfig->regrPath.find(".forest") == std::string::npos)? 
                    myConfig->regrPath: 
                    myConfig->regrPath.substr(0, myConfig->regrPath.length()-7);
    myConfig->regrPath = myConfig->cleanedPath + ".forest";
    myConfig->featsPath = myConfig->cleanedPath + ".data";
    myConfig->regrDetPath = myConfig->cleanedPath + ".det.forest";
    myConfig->featsDetPath = myConfig->cleanedPath + ".det.data";
    myConfig->lookupPath = myConfig->cleanedPath + ".lookup";

    std::string feats = (options[FEATS])? options[FEATS].arg: "11111111111";
    if (feats.size() < 11)
    {
        std::cerr << "Not enough features given: " << feats << '\n';
        exit(0);
    }
    myConfig->featGroups = std::vector<bool>(feats.size(), true);
    for (size_t beg = 0; beg != feats.size(); ++beg)
        myConfig->featGroups[beg] = (feats[beg] == '1');
    
    std::string feats2 = (options[FEATS2])? options[FEATS2].arg: "111111111111111111111";
    if (feats2.size() < 20)
    {
        std::cerr << "Not enough features given: " << feats2 << '\n';
        exit(0);
        }
    myConfig->singleFeats = std::vector<bool>(feats2.size(), true);
    for (size_t beg = 0; beg != feats2.size(); ++beg)
        myConfig->singleFeats[beg] = (feats2[beg] == '1');
    myConfig->numFeats = 28;

    myConfig->header = "";
    for (size_t beg =0; beg != myConfig->numFeats-1; ++beg)
        myConfig->header += "feat" + std::to_string(beg) + "\t";
    myConfig->header += "gold\n";

    myConfig->idxs = std::vector<double>(1500);
    for (size_t beg = 0; beg != 1500; ++beg)
        myConfig->idxs[beg] = (double)beg;
}

