#include "eval.ih"

void Eval::addDistance(string *sent1, size_t len1, string *sent2, size_t len2)
{
    std::vector<size_t> col(len2+1), prevCol(len2+1);

    for (size_t i = 0; i < prevCol.size(); i++)
        prevCol[i] = i;
    for (size_t i = 0; i < len1; i++)
    {
        col[0] = i+1;
        for (size_t j = 0; j < len2; j++)
            col[j+1] = std::min({ prevCol[1 + j] + 1, col[j] + 1,
                                prevCol[j] + (sent1[i]==sent2[j] ? 0 : 1) });
        col.swap(prevCol);
    }

    d_wordDistance += prevCol[len2];
    d_wordTotal += len2;
}
