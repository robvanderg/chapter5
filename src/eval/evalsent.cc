#include "eval.ih"

void Eval::evalSent(vector<vector<string>> cands, vector<string> origs, vector<string> cors, vector<bool> oov)
{
    if (d_origs.size() != d_cors.size())
    {
        cerr << "Error in allignment between original" 
             << "sentence and normalization\n";
        exit(1);
    }

    d_bestSeq.clear();
    d_cors = cors;
    d_origs = origs;
    
    for (d_wordIdx = 0; d_wordIdx != origs.size(); ++d_wordIdx)
        d_bestSeq.push_back((cands[d_wordIdx].size() == 0)? 
                        origs[d_wordIdx]: cands[d_wordIdx][0]); 
    errorRates();

    for (d_wordIdx = 0; d_wordIdx != origs.size(); ++d_wordIdx)
    {
        if (d_goldErrDet && d_origs[d_wordIdx] == d_cors[d_wordIdx])
            continue;
        if (d_goldErrDet2 && oov[d_wordIdx] == false)
            continue;
        ++d_totalWords;
        d_totalCands += cands[d_wordIdx].size();

        errorDetection();
        recall(cands[d_wordIdx]);
        f1(cands[d_wordIdx]); 
    }
}
