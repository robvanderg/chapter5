#include "eval.ih"

Eval::Eval(bool verbose, bool goldErrDet, bool goldErrDet2)
:
    d_verbose(verbose), 
    d_goldErrDet(goldErrDet),
    d_goldErrDet2(goldErrDet2)
{
    d_recall = vector<size_t>(1500, 0);
}
