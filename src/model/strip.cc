#include "model.ih"

void Model::strip(string *word)
{
    while(word->size() > 0 && word->front() == ' ')
        word->erase(0, 1);
    while(word->size() > 0 && word->back() == ' ')
        word->erase(word->size()-1, 1);
}

