#include "model.ih"

void Model::addCands(string const *cands, double const *vals, size_t const numCands, bool check)
{
    for (size_t candIdx = 0; candIdx != numCands; ++candIdx)
    {
        string curCand(cands[candIdx]);
        strip(&curCand);
        while(curCand.front() == ' ')
            curCand = curCand.substr(1);
        while(curCand.back() == ' ')
            curCand = curCand.substr(0, curCand.size() -1);
        if (!d_config->caps)
            std::transform(curCand.begin(), curCand.end(), curCand.begin(), ::tolower);
        if (d_config->onlyCanon == 1 && check && !d_lookup.isCanon(curCand))
            continue;
        if (d_config->onlyCanon == 2 && check && !d_lookup.isCanon(curCand) && !d_dict.contains(curCand))
            continue;
        if (d_config->goldErrDet && curCand == d_origs[d_wordIdx])
            continue;
        if (d_config->numCands > 0 && curCand.find(' ') != string::npos)
            continue; //..TODO
        double curVal(vals[candIdx]);
        auto got = d_curCands.find(curCand);
        if (got == d_curCands.end())
        {
            size_t candId = d_cands[d_wordIdx].size();
            d_cands[d_wordIdx].push_back(curCand);
            d_curCands.emplace(curCand, candId);
            d_featVals[d_wordIdx].resize((candId +1) * d_config->numFeats);
            d_featVals[d_wordIdx][candId * d_config->numFeats + d_featIdx] = curVal;
        }
        else 
        {
            size_t candId = got->second;
            if (d_featVals[d_wordIdx][candId * d_config->numFeats + d_featIdx] == 0.0)
                d_featVals[d_wordIdx][candId * d_config->numFeats + d_featIdx] = curVal; 
        }
    }
}
