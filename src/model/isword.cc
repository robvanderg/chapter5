#include "model.ih"

bool Model::isWord(string const &word)
{//TODO what if we do not use capitals?
    if (word[0] == '@')
        return false;
    if (word[0] == '#')
        return false;
    if (word.substr(0, 4) == "http" || word.substr(0,4) == "www.")
        return false;
    for (string nonWord: {"-user-", "Username", "username", "Urlname", "urlname", "-RRB-", "-LRB-", "-rrb-", "-lrb-", "<URL>", "<USERNAME>"})
        if (word == nonWord)
            return false;

    for (size_t beg = 0; beg != word.size(); ++beg)
        if(isalpha(word[beg]))
            return true;
    return false;
}

