#include "model.ih"

void Model::test(istream *in, ostream *out, size_t skip, size_t numTest)
{
    if (!d_config->goldTreebank.empty())
        d_treebank.open(d_config->goldTreebank);

    for (size_t beg = 0; beg != skip; ++beg)
        readGold(in);

    d_forest = loadForest(false, false);
    if (d_config->sepErrDet)
        d_forestDet = loadForest(false, true);

    size_t sents = 0;
    auto start_time = chrono::high_resolution_clock::now();
    for (;(numTest == 0 || sents != numTest) && readGold(in); ++sents)
    {
        if (d_config->sepErrDet)
            errDet();
        d_cands.clear();
        d_featVals.clear();
        d_results.clear();
        for (d_wordIdx = 0; d_wordIdx < d_origs.size(); ++d_wordIdx)
            gen(false);
        rank();
        d_eval.evalSent(d_cands, d_origs, d_cors, d_oov);
    }

    auto end_time = chrono::high_resolution_clock::now();
    d_eval.setTestSecs(chrono::duration_cast<chrono::seconds>
                            (end_time - start_time).count());
    d_eval.print(out);
}

