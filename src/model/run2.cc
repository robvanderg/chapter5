#include "model.ih"

string Model::run2(string input, double weight)
{
    d_config->weight = weight;
    istringstream in(input);
    string res = "";

    while (readRaw(&in))
    {
        d_cands.clear();
        d_featVals.clear();
        d_results.clear();

        for (d_wordIdx = 0; d_wordIdx < d_origs.size(); ++d_wordIdx)
            gen(false);
        rank();

        for (d_wordIdx = 0; d_wordIdx < d_origs.size(); ++d_wordIdx)
            res += (d_cands[d_wordIdx].empty()? d_origs[d_wordIdx] : d_cands[d_wordIdx][0])  + " ";
        res += '\n';
    }
    return res;
}

