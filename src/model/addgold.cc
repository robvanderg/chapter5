#include "model.ih"

void Model::addGold()
{
    for (size_t corIdx = 0; corIdx != d_cands[d_wordIdx].size(); ++corIdx)
        if (d_cands[d_wordIdx][corIdx] == d_cors[d_wordIdx])
            d_featVals[d_wordIdx][corIdx * d_config->numFeats + d_featIdx] = 1.0;
    ++d_featIdx;
}

