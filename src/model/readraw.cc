#include "model.ih"

bool Model::readRaw(istream *input)
{
    d_origs.clear();
    string line = "-";
    if (d_config->wordPerLine)
    {
        while (true)
        {
            if(!getline((*input), line))
                return !d_origs.empty();
            if (line == "" || line == " ")
                return true;
            if (!d_config->caps)
                transform(line.begin(), line.end(), line.begin(), ::tolower);
            d_origs.push_back(line);
        }
    }

    else if (getline((*input), line))
    {
        if (!d_config->caps)
            transform(line.begin(), line.end(), line.begin(), ::tolower);
        string word;
        stringstream lineStream(line);
        while (lineStream >> word)
        {
            if (d_config->tokenize)
                for (string token: tokenize(word, ".\"?!&*(){}:;/,~\\"))
                    d_origs.push_back(token);
            else
                d_origs.push_back(word);
        }
        return true;
    }
    return false;
}
