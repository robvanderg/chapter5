#include "model.ih"

void Model::run(istream *in, ostream *out, size_t runBeg, size_t runEnd, bool gold)
{
    d_forest = 0;
    vector<string> emptyVec;
    d_forest = new ForestProbability;
    d_forest->initCpp("gold", MEM_DOUBLE, d_config->header, // depvar, memMode, inputFile
            0, d_config->featsPath, 500, //mtry, outprefix, numTrees
            &cerr, d_config->seed, d_config->numThreads, // verboseOut, seed, nthreads, 
            d_config->regrPath, DEFAULT_IMPORTANCE_MODE, //forestPath, importance measure
            0, "", emptyVec, //targetpartitionsize?, splitweightsfile, alwayssplitvars
            "", true, emptyVec, // statusvarname, replace, catvars
            false, DEFAULT_SPLITRULE, "", //savemem, splitrule, caseweights
            false, 1, DEFAULT_ALPHA, DEFAULT_MINPROP, //predall, fraction, minprob
            false, DEFAULT_PREDICTIONTYPE); // holdout, predictiontype

    for (size_t sentIdx = 0; (gold && readGold(in)) || (!gold && readRaw(in)); ++sentIdx)
    {
        if (sentIdx < runBeg || (runEnd != 0 && sentIdx >= runEnd))
            continue;
        d_cands.clear();
        d_featVals.clear();
        d_results.clear();
        for (d_wordIdx = 0; d_wordIdx < d_origs.size(); ++d_wordIdx)
            gen(false);
        rank();
        write(out);
    }
}

