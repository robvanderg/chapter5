#include "model.ih"

void Model::sortCands()
{
    vector<pair<double, string>> rankedCands;
    for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
    {   
        if (!d_config->caps)
            transform(d_cands[d_wordIdx][candIdx].begin(), d_cands[d_wordIdx][candIdx].end(), 
                                d_cands[d_wordIdx][candIdx].begin(), ::tolower);
        rankedCands.push_back(make_pair(d_results[d_wordIdx][candIdx],
                             d_cands[d_wordIdx][candIdx]));
    }
    // assume first cand = orig word
    rankedCands[0].first = rankedCands[0].first * d_config->weight;

    // sort reverse, to get highest score up in the list
    sort(rankedCands.rbegin(), rankedCands.rend());

    for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
    {
        d_cands[d_wordIdx][candIdx] = rankedCands[candIdx].second;
        d_results[d_wordIdx][candIdx] = rankedCands[candIdx].first;
    }
}
