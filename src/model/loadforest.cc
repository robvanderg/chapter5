#include "model.ih"

Forest *Model::loadForest(bool train, bool errDet)
{
    string inputFile = train? d_config->featsPath : d_config->header;
    string outPrefix = train? d_config->cleanedPath : d_config->featsPath;
    string forestPath = train? "": d_config->regrPath;
    if (errDet)
    {
        inputFile = train? d_config->cleanedPath + ".det.data": d_config->header;
        outPrefix = train? d_config->cleanedPath + ".det": d_config->featsDetPath;
        forestPath = train? "": d_config->regrDetPath;
    }
    
    Forest *forest;
    forest = new ForestProbability;
    vector<string> emptyVec;
    forest->initCpp("gold", MEM_DOUBLE, inputFile, // depvar, memMode, inputFile
                0, outPrefix, d_config->numTrees, //mtry, outprefix, numTrees
                &cerr, d_config->seed, d_config->numThreads, // verboseOut, seed, nthreads
                forestPath, DEFAULT_IMPORTANCE_MODE,//IMP_GINI, // forestPath, importance measure
                0, "", emptyVec, // targetpartitionsize?, splitweightsfile, alwayssplitvars
                "", true, emptyVec, // statusvarname,replace, catvars
                false, DEFAULT_SPLITRULE, "", //savemem, splitrule, caseweights
                false, 1, DEFAULT_ALPHA, DEFAULT_MINPROP, //predall, fraction, minProb
                false, DEFAULT_PREDICTIONTYPE); // holdout, predictiontype
    return forest;
}
