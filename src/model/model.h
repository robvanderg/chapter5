#ifndef INCLUDED_MODEL_
#define INCLUDED_MODEL_

#include "../lookup/lookup.h"
#include "../embeds/embeds.h"
#include "../aspgen/aspgen.h"
#include "../ngram/ngram.h"
#include "../ranger/Forest.h"
#include "../vocab/vocab.h"
#include "../config/config.h"
#include "../eval/eval.h"

#include <string>
#include <unordered_map>
#include <vector>
#include <fstream>

class Model
{
    // perm data
    Config *d_config;
    Embeds d_w2v;
    AspGen d_asp;
    NGram d_twitter;
    NGram d_google;
    Vocab d_knowns;
    Vocab d_dict;
    Lookup d_lookup;
    Eval d_eval;
    std::ifstream d_treebank;
    Forest *d_forest;
    Forest *d_forestDet;
    std::vector<bool> d_skip;

    // Original data
    std::vector<std::string> d_origs;
    std::vector<bool> d_oov;
    std::vector<std::string> d_cors;

    // Resulting data
    std::vector<std::vector<std::string>> d_cands;
    std::vector<std::vector<double>> d_featVals;
    std::vector<std::vector<double>> d_results;

    // data per word
    std::unordered_map<std::string, size_t> d_curCands;
    size_t d_featIdx;
    size_t d_wordIdx;

    public:
        Model(Config *config);

        void train(std::string const &in, std::ostream *out);
        void train2(std::string const &in, std::ostream *out);
        void train3(std::string const &in, std::ostream *out);
        void test(std::istream *in, std::ostream *out, size_t skip=0, size_t numTest = 0);
        void run(std::istream *in, std::ostream *out, size_t runBeg = 0, size_t runEnd = 0, bool gold = false);
        void interactive();
        void kFold(const std::string &path, std::ostream *out);
        std::string run2(std::string input, double weight);
        void loadForest();
        Forest *loadForest(bool train, bool errDet);

    private:
        // input data
        bool readRaw(std::istream *input);
        bool readGold(std::istream *input);
        bool consider();
        bool isWord(std::string const &word);
        std::vector<std::string> tokenize(const std::string &source, const std::string &del);

        // generate features
        void errDet();
        void gen(bool errDet);
        void split();
        void addCands(std::string const *cands, double const *vals, size_t const numCands, bool check);
        void addNgrams(NGram *langModel);
        void addGold();
        void genLookup(std::istream *in, size_t numSents);
        bool match(std::string const &orig, std::string const &cand);
        void writeFeats(std::ofstream *out);

        // evaluation/output
        void rank();
        void sortCands();
        void write(std::ostream *out);
        std::string toFsa();

        // utils
        void splitString(const std::string &s, char delim, std::vector<std::string> &elems);
        void strip(std::string *word);
};

#endif
