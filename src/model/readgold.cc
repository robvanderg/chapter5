#include "model.ih"

bool Model::readGold(istream *input)
{
    d_origs.clear();
    d_oov.clear();
    d_cors.clear();
    string line;
    bool gotLine = false;
    while(getline((*input), line))
    {
        gotLine = true;
        vector<string> splitted;
        splitString(line, '\t', splitted);
        if (splitted.size() <= 1)
            break;
        if (splitted.size() == 2)
            splitted.push_back("");
        for(size_t wordIdx = 3; wordIdx < splitted.size(); ++ wordIdx)
            splitted[2] += " " + splitted[wordIdx];
            
        strip(&splitted[0]);
        strip(&splitted[2]);
        if (!d_config->caps)
        {
            transform(splitted[0].begin(), splitted[0].end(), 
                        splitted[0].begin(), ::tolower);
            transform(splitted[2].begin(), splitted[2].end(), 
                        splitted[2].begin(), ::tolower);
        }

        d_origs.push_back(splitted[0]);
        d_oov.push_back(splitted[1]!="-");
        d_cors.push_back(splitted[2]);
    }
    return gotLine;
}

