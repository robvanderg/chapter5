#include "model.ih"

void Model::kFold(const string &path, ostream *out)
{
    if (d_config->kfold == 0)
    {
        cerr << "Error: gave option --kfold, but did not specify k!\n";
        exit(1);
    }
    ifstream in(path);
    if (!in.good())
    {
        cerr << "Could not read training file: " << path << '\n';
        exit(1);
    }

    size_t numSents = 0;
    while(readGold(&in))
        ++numSents;
    in.close();

    size_t split = numSents / d_config->kfold;
    cerr << "Spliting: " << numSents << " sents in pieces of " << split << '\n';
    (*out) << '\n';
    for (size_t fold = 0; fold != d_config->kfold; ++fold)
    {
        size_t skipBeg = fold * split;
        size_t skipEnd = (d_config->kfold -1 == fold)?numSents:fold * split + split;
        cerr << "Split: " << skipBeg << '\t' << skipEnd << '\n';
        d_skip = vector<bool>(numSents, false);
        fill(d_skip.begin() + skipBeg, d_skip.begin() + skipEnd, true);
       
        if (fold > 0)
            d_config->cleanedPath.pop_back();
        d_config->cleanedPath = d_config->cleanedPath + to_string(fold);
        d_config->regrPath = d_config->cleanedPath + ".forest";
        d_config->lookupPath = d_config->cleanedPath + ".lookup";
        d_config->featsPath = d_config->cleanedPath + ".data";

        train(path, out);
        
        in.open(path);
        run(&in, out, skipBeg, skipEnd, true);
        out->flush();
        in.close();
    }
}
