#include "model.ih"

Model::Model(Config *config)
:
    d_config(config),
    d_w2v(config->w2v),
    d_asp(config->aspLan, config->aspDict, config->aspMode),
    d_twitter(config->twitter, true),
    d_google(config->wiki, true),
    d_knowns(config->knowns, false),
    d_dict(config->dict, false), //TODO check!
    d_lookup(((config->train)?"":config->lookupPath)),
    d_eval(d_config->verbose, d_config->goldErrDet, d_config->goldErrDet2 && !d_config->sepErrDet)
{
}

