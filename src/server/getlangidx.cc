#include "server.ih"

int Server::getLangIdx(string const &language)
{
    for (size_t langId = 0; langId != d_langs.size(); ++langId)
        if (d_langs[langId] == language)
            return langId;
    return 0; //default is first lang
}
