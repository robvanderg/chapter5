#include "server.ih"

void Server::run()
{
    while(true)
    {
        string input = getInput();
        cout << input << '\n';
        if (input == "")
            continue;

        if (input[0] == 'N')
            handleNorm(input);
        else if (input[0] == 'F')
            handleFeedback(input);
    }
}

