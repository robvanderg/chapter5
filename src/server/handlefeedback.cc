#include "server.ih"

void Server::handleFeedback(string const &input)
{
    vector<string> inputVec;
    split(input, d_glue, inputVec);
    if (inputVec.size() < 6)
        return;

    std::ofstream outfile("/net/aistaff/rob/feedback.txt", std::ios_base::app);
    outfile << d_glue << input << d_glue << '\n'; 
    outfile.close();
}
