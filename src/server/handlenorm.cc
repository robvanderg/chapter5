#include "server.ih"

void Server::handleNorm(string const &input)
{
    vector<string> inputVec;
    split(input, d_glue, inputVec);
    if (inputVec.size() < 4)
    {
        sendBack("Sorry, your input is in the incorrect format\n");
        return;
    }
    int langIdx = getLangIdx(inputVec[1]);
    double weight = getWeight(inputVec[2]);
    string norm = d_models[langIdx]->run2(inputVec[3], weight);
    sendBack(norm);

    std::ofstream outfile("/net/aistaff/rob/input.txt", std::ios_base::app);
    outfile << d_glue << input << d_glue << norm << d_glue << '\n';
    outfile.close();

}
