#ifndef INCLUDED_SERVER_
#define INCLUDED_SERVER_

#include <string>
#include <vector>
#include <netinet/in.h>
#include <sys/socket.h>


#include "../model/model.h"

class Server
{
    std::vector<Model*> d_models;
    std::vector<std::string> d_langs;

    char d_glue;
    int d_serverSockFD, d_clientSockFD, d_bufferSize;
    struct sockaddr_storage cli_addr;
    struct sockaddr_in serv_addr;

    public:
        Server(std::vector<std::string> languages, std::vector<Model*> models, int port);
        ~Server();

        void run();

    private:
        std::string getInput();
        bool checkIp(char *ipstr);

        void handleNorm(std::string const &input);
        int getLangIdx(std::string const &input);
        double getWeight(std::string const &input);
        std::string normalize(std::string const &txt, int langIdx, double weight);
        void sendBack(std::string output);

        void handleFeedback(std::string const &input);

        void split(const std::string &s, char delim, std::vector<std::string> &elems);
};
        
#endif
