#include "lookup.ih"

bool Lookup::contains(string const &word)
{
    auto got = d_data.find(word);
    return (got != d_data.end());
    
    /*if (got == d_data.end())
        return false;
    auto got2 = got->second.find(got->first);
    if (got2 != got->second.end() && got->second.size() > 1)
        return true;
    return false;*/
}
