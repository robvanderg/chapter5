#include "lookup.ih"

void Lookup::addWord(string const &src, string const &tgt)
{
    auto got = d_data.find(src);
    if(got == d_data.end())
    {
        map<string, double> results;
        results[tgt] = 1;
        d_data[src] = results;
    }
    else
    {
        auto target = got->second.find(tgt);
        if (target == got->second.end())
            got->second[tgt] = 1;
        else
            got->second[tgt] = got->second[tgt] + 1;
    }
    d_knowns.addWord(tgt);
}
