#include "lookup.ih"

map<string, double> Lookup::getLookups(string const &word)
{
    auto got = d_data.find(word);
    if (got == d_data.end())
        return map<string, double>();
    else
        return got->second;
}
