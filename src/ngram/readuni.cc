#include "ngram.ih"

void NGram::readUni(string path, uint32_t minCount)
{
    map<string, uint64_t> counts;
    counts.emplace(d_beg, 0);
    counts.emplace(d_end, 0);
    string line, word;
    ifstream in(path);
    if (!in.good())
    {
        cerr << "Could not read unigrams: " << path << '\n';
        exit(1);
    }
    while(getline(in, line))
    {
        istringstream iss(line);
        ++counts[d_beg];
        ++counts[d_end]; // TODO, can be done more efficient?
        while(iss >> word)
        {
            auto got = counts.find(word);
            if (got == counts.end())
                counts.emplace(word, 1);
            else 
                ++got->second;
        }
    }
    in.close();

    d_unigramCounts.push_back(0);
    for (std::map<string,uint64_t>::iterator it=counts.begin(); it!=counts.end(); ++it)
        if (it->second >= minCount)
        {
            d_unigrams.addOrdered(it->first);
            d_unigramCounts.push_back(it->second);
        }
}
