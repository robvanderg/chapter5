#include "ngram.ih"

void NGram::learn(string path, uint32_t minCount)
{
    cerr << "Reading unigrams\n";
    readUni(path, minCount);
    cerr << d_unigrams.size() << " unigrams found\n\n";   
 
    cerr << "Reading bigrams\n";
    readBi(path, minCount);
    cerr << d_bigrams.size() << " bigrams found\n\n";
}
