#include "ngram.ih"

NGram::NGram(string path, bool bin)
{
    if (bin)
        loadBin(path);
    else
        load(path);
}
