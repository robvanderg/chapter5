#include "ngram.ih"

void NGram::saveBin(string path)
{
    cerr << "Writing: " << path << '\n';
    ofstream ofs(path, ios::binary);

    if (!ofs.good())
    {
        cerr << "Could not write ngrams: " << path << '\n';
        exit(1);
    }
    d_unigrams.saveBin(&ofs);

    uint64_t size = d_unigramCounts.size();
    ofs.write(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    ofs.write(reinterpret_cast<char*>(&d_unigramCounts[0]), sizeof(uint64_t) * d_unigramCounts.size());

    size = d_bigrams.size();
    ofs.write(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    ofs.write(reinterpret_cast<char*>(&d_bigrams[0]), sizeof(uint64_t) * d_bigrams.size());

    size = d_bigramCounts.size();
    ofs.write(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    ofs.write(reinterpret_cast<char*>(&d_bigramCounts[0]), sizeof(uint32_t) * d_bigramCounts.size());

    ofs.close();
}

