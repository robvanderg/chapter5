
pred = []
#for line in open('test.txt', errors='ignore'):
for line in open('lexnorm.U.6.4.2.txt', errors='ignore'):
    pred.append(line.split())

raw = []
gold = []
idx = 0

cor = 0
inCor = 0
needNorm = 0
for line in open('../data/en/lexnorm1.2', errors='ignore'):
    tok = line.split()
    if line.strip().isdigit() and len(raw) > 0:
        if (len(raw) != len(pred[idx])):
            if "�" in pred[idx]:
                charIdx = pred[idx].index("�")
                pred[idx] = pred[idx][0:charIdx] + pred[idx][charIdx+1:len(pred[idx])]
        if (len(raw) == len(pred[idx])):#TODO, still miss 4 sents
            for wordIdx in range(len(raw)):
                if raw[wordIdx].lower() != pred[idx][wordIdx].lower():
                    needNorm += 1
                if gold[wordIdx].lower() == pred[idx][wordIdx].lower() or raw[wordIdx] == ')' or raw[wordIdx] == '(':
                    cor += 1
                else:
                    inCor += 1
                    print(raw[wordIdx], gold[wordIdx], pred[idx][wordIdx]) 
        raw = []
        gold = []
        idx += 1
    elif len(tok) == 3:
        raw.append(tok[0])
        gold.append(tok[2])

print('cor', str(cor))
print('inCor', str(inCor))
print('needNorm', str(needNorm))
acc = cor / (cor + inCor)
print('acc', str(acc))
base = 1- (needNorm / (cor + inCor))
print('base', str(base))
print('err', str((acc-base)/(1-base)))

