#include "vocab.ih"

uint32_t Vocab::getId(uint32_t beg, uint32_t end, char const *searchWord)
{
    uint32_t split = (end + beg) / 2;
    uint32_t begIdx = d_idxs[split];

    int comp = strcmp(searchWord, &d_vocab[begIdx]);
    
    //TODO this should be possible with less ifs
    if (beg == split && end == beg + 1)
        return getId(end, end, searchWord);
    if (beg == split && split == end)
        return (comp ==0)?beg:0;
    if (beg == end || beg == split)
        return 0;
    if (comp > 0)
        return getId(split, end, searchWord);
    if (comp < 0)
        return getId(beg, split, searchWord);
    return split;
}
