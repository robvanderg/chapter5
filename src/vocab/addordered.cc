#include "vocab.ih"

void Vocab::addOrdered(string const &word)
{
    d_idxs.push_back(d_vocab.size());
    d_vocab.resize(d_vocab.size() + word.size() + 1);
    memcpy (&d_vocab[d_vocab.size() - word.size() -1], &word[0], sizeof(char) * word.size());
    d_vocab.back() = '\0';
}
