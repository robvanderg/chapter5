#include "vocab.ih"

void Vocab::load(string const &path)
{
    cerr << "Loading: " << path << '\n';
    if (path == "")
        return;
    ifstream in(path);
    if (!in.good())
    {
        cerr << "Could not read vocab: " << path << '\n';
        exit(1);
    }

    load(&in);
}
