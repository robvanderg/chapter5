#include "vocab.ih"

void Vocab::optimize()
{
    if (d_idxs.size() > 1)
        for (size_t beg = 1; beg != d_idxs.size(); ++beg)
            d_collect.insert(string(&d_vocab[d_idxs[beg]]));
    d_vocab.clear();
    d_idxs.clear();

    d_idxs.push_back(0);
    for (string str: d_collect)
    {
        d_idxs.push_back(d_vocab.size());
        d_vocab.resize(d_vocab.size() + str.size() + 1);
        memcpy (&d_vocab[d_vocab.size() - str.size() - 1], &str[0], sizeof(char) * str.size());
    }   
    d_collect.clear();
}
