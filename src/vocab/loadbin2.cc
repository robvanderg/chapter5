#include "vocab.ih"

void Vocab::loadBin(ifstream *ifs)
{
    uint64_t size;
    ifs->read(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    d_vocab = vector<char>(size);
    ifs->read(&d_vocab[0], sizeof(char) * size);
    
    ifs->read(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    d_idxs = vector<uint32_t>(size);
    ifs->read(reinterpret_cast<char*>(&d_idxs[0]), sizeof(uint32_t) * size);
}
