#include "config/main.ih"
#include "server/server.h"

void server()
{
    std::vector<Model*> models;
    std::vector<std::string> langs;
    
    // English
    std::vector<std::string> arguments = {"-r", "../working/demo.en1", "-d", "../data/en", "-t", "-C",  "-f" "111101111111" "-b"};
    std::vector<char*> argv2;
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    option::Stats  stats(usage, argv2.size()-1, argv2.data());
    std::vector<option::Option> options(stats.options_max);
    std::vector<option::Option> buffer(stats.buffer_max);
    option::Parser parse(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig;
    readConfig(&myConfig, options);
    Model myModel(&myConfig);
    myModel.loadForest();
    models.push_back(&myModel);
    langs.push_back("en");

    // Dutch
    arguments[1] = "../working/demo.nl";
    arguments[3] = "../data/nl";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig2;
    readConfig(&myConfig2, options);
    Model myModel2(&myConfig2);
    myModel2.loadForest();
    models.push_back(&myModel2);
    langs.push_back("nl");
    
    // Spanish
    arguments[1] = "../working/demo/demo.es";
    arguments[3] = "../data/es";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig6;
    readConfig(&myConfig6, options);
    Model myModel6(&myConfig6);
    myModel6.loadForest();
    models.push_back(&myModel6);
    langs.push_back("es");
    
    arguments.pop_back(); //no bad model!
    // Croatian
    arguments[1] = "../working/demo/demo.hr";
    arguments[3] = "../data/hr";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig3;
    readConfig(&myConfig3, options);
    Model myModel3(&myConfig3);
    myModel3.loadForest();
    models.push_back(&myModel3);
    langs.push_back("hr");
    
    // Serbian
    arguments[1] = "../working/demo.sr";
    arguments[3] = "../data/sr";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig4;
    readConfig(&myConfig4, options);
    Model myModel4(&myConfig4);
    myModel4.loadForest();
    models.push_back(&myModel4);
    langs.push_back("sr");
    
    // Slovenian
    arguments[1] = "../working/demo.sl";
    arguments[3] = "../data/sl";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig5;
    readConfig(&myConfig5, options);
    Model myModel5(&myConfig5);
    myModel5.loadForest();
    models.push_back(&myModel5);
    langs.push_back("sl");

    Server server(langs, models, 8856);
    std::cout << "loaded all\n";
    server.run();
}
