import sys

if len(sys.argv) < 2:
    print("give feature file!")

featFile = open(sys.argv[1])

vals = []
zeros = []
for feat in featFile.readline().split():
    vals.append([])
    zeros.append(0)

for line in featFile:
    idx = 0
    for val in line.split():
        vals[idx].append(float(val))
        if (float(val) == 0.0):
            zeros[idx] = zeros[idx] + 1
        idx += 1
print(str(len(vals[0])))
print("       \tzeros  \ttotal")
for featIdx in range(len(vals)):
    total = 0
    for item in vals[featIdx]:
        total += item
    print(str(featIdx) + "\t" + str(zeros[featIdx]) + "\t" + str(round(total,2)))
