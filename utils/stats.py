import sys
import os

if len(sys.argv) < 2:
    print('please give input files')


for i in range(1, len(sys.argv)):
    print(sys.argv[i])
    aspell = sys.argv[i][:sys.argv[i].rfind('/') + 1] + 'aspell'
    aspDict = set()
    if os.path.isfile(aspell):
        for line in open(aspell):
            aspDict.add(line.strip())
    eq = 0
    neq = 0
    double = 0
    split = 0
    merge = 0
    sents = 0
    aspOrig = 0
    aspGold = 0
    prevNeq = False
    for line in open(sys.argv[i], encoding='utf-8', errors='ignore'):
        splitted = line.split('\t')
        if len(splitted) == 2:
            print(line)
        elif len(splitted) < 2:
            sents += 1
            prevNeq = False
            pass
        else:
            orig = splitted[0].strip().lower()
            cor = splitted[2].strip().lower()
            if orig == cor:
                eq += 1
                prevNeq = False
            else:
                neq += 1
                if prevNeq:
                    double += 1
                prevNeq = True
                #print(orig, cor)
            if cor.find(' ') > 0:
                #print(orig, cor)
                split += 1
            if len(cor.strip()) == 0:
                merge += 1
            if orig in aspDict:
                aspOrig += 1
            #else:
            #    print(orig)
            if cor in aspDict:
                aspGold += 1
    
    words = eq + neq
    print("changed:        ", neq,   '\t', neq / words)
    print("double changed: ", double,   '\t', double / words)
    print("split:          ", split, '\t', split / words)
    print("merge:          ", merge, '\t', merge / words)
    print("sents:          ", sents, '\t', words / sents)
    print("origAspell:     ", aspOrig, '\t', aspOrig / words)
    print("corAspell:      ", aspGold, '\t', aspGold / words)
    print("words:          ", words, '\t')
    print()

