import sys

data = {}
for line in open(sys.argv[1]):
    tok = line.split()
    cat = tok[0]
    repl = '->'.join(tok[-2:])
    count = int(tok[1])
    if cat not in data:
        data[cat] = [0,0]
    data[cat][0] += 1
    data[cat][1] += count


for cat in ['+', '-', 'p', 's', 'n']:
    if cat in data:
        print(cat, data[cat][0], data[cat][1])
    else:
        print(cat, 0, 0)
        

