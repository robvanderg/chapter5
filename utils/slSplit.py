import sys

outL1 = open('l1.norm.xml', 'w')
outL3 = open('l3.norm.xml', 'w')

def write(outL1, outL3, category, text):
    if category == 'L1':
        outL1.write(text)
    elif category == 'L3':
        outL3.write(text)
    else:
        print('Err: category = ' + category)

text = ''
category = ''
for line in open(sys.argv[1]):
    if line.startswith('<text id'):
        if category != '':
            write(outL1, outL3, category, text)
            text = ''
            category = ''
        category = line.split('"')[-2]
    text += line
write(outL1, outL3, category, text)

outL1.close()
outL3.close()
    

