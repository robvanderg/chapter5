import sys

tweets = []

for line in open(sys.argv[1]):
    if line[0].isnumeric():
        tweets.append(line)

gold = open(sys.argv[2]).readlines()
mine = open(sys.argv[3]).readlines()


tweetIdx = 0
print(tweets[0].strip())
for lineIdx in range(len(gold)):

    if len(gold[lineIdx].split()) < 3:
        tweetIdx += 1
        if tweetIdx >= len(tweets):
            exit(1)
        print(tweets[tweetIdx][:-1])
    else:
        orig = gold[lineIdx].split()[0]
        norm = mine[lineIdx].strip()
        cat = gold[lineIdx].split()[1]
        if cat != '-':
            if norm != orig:
                print('\t' + orig + ' ' + cat + ' ' + norm)
            else:
                print('\t' + orig + ' ' + cat + ' -')



