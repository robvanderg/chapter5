import sys

if len(sys.argv) < 3:
    print("please give 2 files, I will calculate the number of tokenization and normalization issues")


orig = open(sys.argv[1]).readlines()
normed = open(sys.argv[2]).readlines()

normC = 0
punctC = 0
for i in range(len(orig)):
    origSet = set(orig[i].split())
    normSet = set(normed[i].split())
    onlyNormWord = []
    onlyNormPunct = []
    onlyOrig = []
    for word in normSet:
        if word not in origSet:
            isWord = False
            for char in word:
                if char.isalnum():
                    isWord = True
            if isWord:
                onlyNormWord.append(word)
            else:
                onlyNormPunct.append(word)
    for word in origSet:
        if word not in normSet:
            onlyOrig.append(word)

    founds = 0
    illegal = False
    for punct in onlyNormPunct:
        found = False
        found2 = False
        found3 = False
        for word in onlyNormWord:
            if word + punct in onlyOrig or punct + word in onlyOrig:
                found = True
        if not found:
            for word in onlyOrig:
                if punct in word:
                    found2 = True
        # probably norm:
        # - & ...
        if not found and not found2 and (punct == '-' or punct == '&' or punct == '...'):
            found3 = True
        if not found and not found2 and not found3:#ERROR!
            illegal = True
        else:
            founds+= 1
    if not illegal:
        size = min(len(onlyNormWord), len(onlyOrig))
        punctC += founds
        normC += (size - founds)
    
print('norm', normC)
print('punct', punctC)
