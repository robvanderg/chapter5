import sys

if len(sys.argv) < 2:
    print("please specify input file")
    exit(1)

for line in open(sys.argv[1]):
    if line.startswith('<s>'):
        print()
    elif line.startswith('<'):
        continue
    else:
        print(line.split()[0] + '\tALL\t' + line.split()[1])



