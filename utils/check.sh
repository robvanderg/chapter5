if ps aux | grep "/demo/bin/monoise" | grep -v grep > /dev/null
then
    echo "Running" >> /net/shared/rob/monoise/status
else
    echo "Stopped" >> /net/shared/rob/monoise/status
    date >> /net/shared/rob/monoise/status
    cd /net/shared/rob/monoise/src
    ./demo/bin/monoise -m DE >> /net/shared/rob/monoise/stdout 2>> /net/shared/rob/monoise/stderr &
fi

