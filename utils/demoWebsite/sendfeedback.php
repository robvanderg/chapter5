<?php
function sendFeedback($feedback, $language, $weight, $input, $output){
    $glue = chr(23);
    $inputString = "F".$glue.$language.$glue.$weight.$glue.$feedback.$glue.$input.$glue.$output;

    error_reporting(E_ALL);

    /* Get the port for the WWW service. */
    $service_port = 8856;

    /* Get the IP address for the target host. */
    $address = gethostbyname('haytabo.let.rug.nl');

    /* Create a TCP/IP socket. */
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {
        return "ERROR: socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n" . "Please report this bug: r.van.der.goot@rug.nl\n";
    }

    $result = socket_connect($socket, $address, $service_port);
    if ($result === false) {
        return "ERROR: socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n" . "Please report this bug: r.van.der.goot@rug.nl\n";
    }

    socket_write($socket, $inputString, 600);

    return "Thanks for your feedback!";
}
?>

