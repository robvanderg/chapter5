import myutils

limits = [7575,5637,32505,35216,44944,54437,56837]
for i in range(1000,max(limits), 1000):
    for langIdx, lang in enumerate(myutils.langs):
        if limits[langIdx] > i:
            cmd = './tmp/bin/binary -m TR -i ../data/' + lang + '/train -r ../working/' + lang + '.' + str(i) + ' -d ../data/' + lang + ' -D ../data/' + lang + '/dev -N ' + str(i) + ' > ../preds/learning.' + lang + '.' + str(i) + '.out 2> ../preds/learning.' + lang + '.' + str(i) + '.err'
            print(cmd)
   
