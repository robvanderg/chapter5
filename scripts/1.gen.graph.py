import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

if len(sys.argv) < 2:
    print("usage:")
    print("grep Upperbound preds/gen*out > results")
    print("python3 scripts/1.gen.graph.py results")
    exit(1)

data ={}

for line in open(sys.argv[1]):
    tok = line.split('/')[-1].split()
    lang = tok[0].split('.')[1]
    feats = tok[0].split('.')[2]
    score = float(tok[-2])

    if lang not in data:
        data[lang] = {'single':[0] * 9, 'abl':[0] * 9, 'all':0}
    if feats.count('1') == 5: # all
        data[lang]['all'] = score
    elif feats.count('1') > 1: # ablation 
        featIdx = feats.find('0')
        data[lang]['abl'][featIdx] = score
    else: # isolation
        featIdx = feats.find('1')
        data[lang]['single'][featIdx] = score

def graph(which, location, prefix, ySize, metric, outName):
    fig, ax = plt.subplots(figsize=(8,5), dpi=300)
    barSize = 1 / (len(myutils.featNames[:5]) + 1)
     
    for featIdx, feat in enumerate(myutils.featNames[:5]):
        x = []
        y = []
        for langIdx, lang in enumerate(myutils.langs):
            y.append(langIdx + (1+featIdx) * barSize)
            x.append(data[lang][which][featIdx] - (0 if which == 'single' else data[lang]['all']))
            if which == 'abl':
                x[-1] = x[-1] * -1
        print(x)
        useColor = myutils.colors[featIdx]
        if featIdx == 4:
            useColor = 'gold'#slategrey
        ax.bar(y, x, width=barSize, color=useColor, label = prefix + feat)
    print()

    if which == 'single':#add `all' bar
        for langIdx, lang in enumerate(myutils.langs):
            if langIdx == 0:
                ax.plot([langIdx, langIdx+1], [data[lang]['all']] * 2, color='gray', linewidth=2, label='All')
            else:
                ax.plot([langIdx, langIdx+1], [data[lang]['all']] * 2, color='gray', linewidth=2)

    myutils.setTicks(ax, myutils.corpora, 45)


    ax.set_xlim(0,len(myutils.corpora))
    ax.set_xlabel('Corpus')
    ax.set_ylim(ySize)
    ax.set_ylabel(metric)

    leg = ax.legend(bbox_to_anchor=(1.34, 1))
    leg.get_frame().set_linewidth(1.5)
    fig.savefig(outName, bbox_inches='tight')
    #plt.show()

graph('single', 'upper right', '', (0,1), 'Recall', 'genSingle.pdf')
graph('abl', 'upper right', '-', (0,.35), 'Decrease in Recall', 'genAbl.pdf')

