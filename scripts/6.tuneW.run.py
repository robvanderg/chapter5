import myutils

for lang in myutils.langs:
    for weight in range(1,11):
        weight = 1.5 + weight * 0.05
        weightStr = str(round(weight,2))
        cmd = './tmp/bin/binary -m TR -i ../data/' + lang + '/train -r ../working/' + lang + '.' + weightStr + ' -d ../data/' + lang + ' -D ../data/' + lang + '/dev -w ' + weightStr + ' > ../preds/tuneW.' + lang + '.' + weightStr + '.out 2> ../preds/tuneW.' + lang + '.' + weightStr + '.err'
        print (cmd)
