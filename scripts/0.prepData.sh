mkdir data
cd data
for lang in en es hr nl sl sr;
do
    if [ ! -d $lang ]; then
        curl www.robvandergoot.com/data/monoise/"$lang".tar.gz | tar xvz
    fi
done


