# This script uses the models trained by graph.sh
# you can also train them by uncommenting line 8

WEIGHTS=("0.125" "0.25" "0.5" 1 2 4 8 16)

for i in {0..9}; 
do 
    #./tmp/bin/binary -m TR -i enData/chenli -r working/chenli$i -a -n 12 -s $i
    for WEIGHT in "${WEIGHTS[@]}";
    do
        echo $i $WEIGHT >> util/table
        head -10 config.en > config.tmp
        mv config.tmp config.en
        echo "-latticeWeight $WEIGHT -gr enData/ewtwsj.gr" >> config.en

        ./tmp/bin/binary -m TE -i ~/data/foster.tokens -r working/chenli$i -p ~/data/foster.ptb -c 1 -a -u >> util/table
        ./tmp/bin/binary -m TE -i ~/data/foster.tokens -r working/chenli$i -p ~/data/foster.ptb -c 1 -a >> util/table
        ./tmp/bin/binary -m TE -i ~/data/foster.tokens -r working/chenli$i -p ~/data/foster.ptb -c 6 -a -u >> util/table
        ./tmp/bin/binary -m TE -i ~/data/foster.tokens -r working/chenli$i -p ~/data/foster.ptb -c 6 -a >> util/table
    done
done

head -10 config.en > config.tmp
mv config.tmp config.en
echo "-latticeWeight 2 -gr enData/ewtwsj.gr" >> config.en

python3.5 util/genTable.py
