import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

if len(sys.argv) < 2:
    print("usage:")
    print("grep \"^ERR\" preds/tuneW.*out > results")
    print("python3 scripts/6.tuneW.graph.py results ")
    exit(1)

data ={}
fig, ax = plt.subplots(figsize=(8,5), dpi=300)

for line in open(sys.argv[1]):
    tok = line.split('/')[-1].split()
    lang = tok[0].split('.')[1]
    topN = float('.'.join(tok[0].split('.')[2:4]))
    err =  float(tok[-1])
    if err < 0.00001 or err > 1:   
        err = 0
    if lang not in data:
        data[lang] = {}
    data[lang][topN] = err

for langIdx, lang in enumerate(myutils.langs):
    x = []
    y = []
    for topNidx, topN in enumerate(sorted(data[lang])):
        x.append(topN)
        y.append(data[lang][topN])
    style = '-'
    if langIdx > 3:
        style= '--'
    print(lang, y)
    ax.plot(x, y, linestyle=style, label=myutils.corpora[langIdx], linewidth=3)
    
ax.set_ylabel('ERR')
ax.set_xlabel('Weight')
leg = ax.legend(loc='upper right', bbox_to_anchor=(1.34, 1))
leg.get_frame().set_linewidth(1.5)
ax.set_ylim((.25,.75))#ymin=0?
fig.savefig('tuneW_f1.pdf', bbox_inches='tight')
#plt.show()
