import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

if len(sys.argv) < 2:
    print("usage:")
    print("grep \"^ERR\" preds/errDet.* > results")
    print("python3 scripts/7.errDet.graph.py results ")
    exit(1)

data ={}
fig, ax = plt.subplots(figsize=(8,5), dpi=300)

for line in open(sys.argv[1]):
    if line.find('voc') > 0:
        continue
    tok = line.split('/')[-1].split()
    tok2 = tok[0].split('.')
    err = float(line.split()[-1])
    if err > 1:
        err = 0
    weight = float('0.' + tok2[3])
    lang = tok2[1]
    if lang not in data:
        data[lang] = {}
    data[lang][weight] = err

x = []
for weight in sorted(data['nl']):
    x.append(weight)
print(x)

for langIdx, lang in enumerate(myutils.langs):
    langData = []
    for weight in x:
        if weight in data[lang]:
            langData.append(data[lang][weight])
        else:
            langData.append(0)
    style = '-'
    if langIdx > 3:
        style= '--'
    print(lang,langData) 
    ax.plot(x[:-1], langData[1:], linestyle=style, label=myutils.corpora[langIdx])


#myutils.setTicks(ax, myutils.corpora, 45)
ax.set_ylabel('ERR')
ax.set_xlim((0,.9))
ax.set_xlabel('Threshold for Error Detection')
leg = ax.legend(loc='upper right')
leg.get_frame().set_linewidth(1.5)
ax.set_ylim((.0,.75))
fig.savefig('errDet.pdf', bbox_inches='tight')
#plt.show()
