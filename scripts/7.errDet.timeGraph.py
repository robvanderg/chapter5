import sys
import matplotlib.pyplot as plt
import myutils

data ={}
fig, ax = plt.subplots(figsize=(8,5), dpi=300)

def getTime(path):
    words = 0.0
    time = 1.0
    for line in open(path):
        if line.startswith('Total'):
            words = float(line.split()[-1])
        if line.startswith('Testing time'):
            time = float(line.split()[-1])
    return int(round(float(words / time) ,0))

for langIdx, lang in enumerate(myutils.langs):
    times = []
    weights = []
    for weight in (range(0,105,5)):#105
        times.append(getTime('preds/errDet.' + lang + '.' + str(weight/100) + '.out'))
        weights.append(weight/100)
    style = '-'
    if langIdx > 3:
        style= '--'
    print(weights) 
    print(times)
    print()
    ax.plot(weights, times, linestyle=style, label=myutils.corpora[langIdx])

#myutils.setTicks(ax, myutils.corpora, 45)
ax.set_ylabel('Words/second')
#ax.set_xlim((0,.9))
ax.set_xlabel('Threshold for Error Detection')
leg = ax.legend(loc='upper left')
leg.get_frame().set_linewidth(1.5)
#ax.set_ylim((.0,.75))
fig.savefig('errDet.pdf', bbox_inches='tight')
plt.show()

