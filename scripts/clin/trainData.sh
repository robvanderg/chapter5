cd src

cp ../data/config.en ../data/config.en.backup

tail -n 15369 ../data/enData/lexnorm2015.train > ../data/enData/lexnorm2015.dev
tail -n 9588 ../data/enData/chenli > ../data/enData/chenli.dev

for i in 100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 
do
    echo "numTrain $i" > numTrain
    tail -n +2 ../data/config.en > config.en.small
    cat numTrain config.en.small > ../data/config.en
    rm numTrain config.en.small
    #./tmp/bin/binary -m TR -i ../data/enData/chenli -r ../working/chenliGold.train."$i"  
    ./tmp/bin/binary -m TE -i ../data/enData/chenli.dev -r ../working/chenliGold.train."$i" > ../runs/chenli.train.$i
    #./tmp/bin/binary -m TR -i ../data/enData/lexnorm2015.train -r ../working/lexnorm2015.train."$i"
    ./tmp/bin/binary -m TE -i ../data/enData/lexnorm2015.dev -r ../working/lexnorm2015.train."$i" > ../runs/lexnorm2015.train.$i
done

mv ../data/config.en.backup ../data/config.en

grep "^Normalization:" ../runs/lexnorm2015.train.* > ../runs/lexnorm2015.train
grep "^Normalization:" ../runs/chenli.train.* > ../runs/chenli.train
python3 ../scripts/clin/trainData.py ../runs/chenli.train ../runs/lexnorm2015.train

