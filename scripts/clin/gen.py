import sys
import matplotlib.pyplot as plt

data ={}
data['lexnorm2015'] = {'single':[0] * 9, 'abl':[0] * 9, 'all':0}
data['chenli'] = {'single':[0] * 9, 'abl':[0] * 9, 'all':0}
data['orphee'] = {'single':[0] * 9, 'abl':[0] * 9, 'all':0}
names = ['w2v', 'asp', 'lookup', 'word.*', 'split', '', '', '']

for line in open(sys.argv[1]):
    line=line.strip()
    corpus = line[line.find('/') + 1:line.find('.')]
    score = float(line[line.rfind(':')+1:line.rfind(' ')])
    feats = line[:line.find(':')]
    feats = feats[-10:]
    if feats.count('1') == 6: # all
        data[corpus]['all'] = score
    elif feats.count('1') > 1: # ablation 
        featIdx = feats.find('0')
        data[corpus]['abl'][featIdx] = score
    else: # isolation
        featIdx = feats.find('1')
        data[corpus]['single'][featIdx] = score

def graph(which, location, prefix, ySize, outName):
    chenli = [data['chenli']['all']] +  data['chenli'][which][0:5]
    lexnorm2015 = [data['lexnorm2015']['all']] + data['lexnorm2015'][which][0:5]
    orphee = [data['orphee']['all']] + data['orphee'][which][0:5]
    print(chenli, len(chenli))
    bar_width = 0.25
    index = range(len(chenli))
    index2 = []
    index3 = []
    for i in index:
        index2.append(i + bar_width)
        index3.append(i + bar_width + bar_width)

    plt.bar(index, chenli, bar_width, color='b', label='LiLiu')
    plt.bar(index2, lexnorm2015, bar_width, color='r', label='LexNorm2015')
    plt.bar(index3, orphee, bar_width, color='y', label='GhentNorm')
    if prefix == '':
        plt.xticks(index2, ['all'] + names[0:5])
    else:
        names2 = []
        for name in names[0:5]:
            names2.append(prefix + name)
        plt.xticks(index2, ['all'] + names2)
    plt.legend(loc=location)
    #plt.xlabel()
    plt.ylim(ySize)
    plt.ylabel('recall')
    plt.savefig(outName, bbox_inches='tight')
    plt.clf()
    plt.cla()

graph('single', 'upper right', '', (0,1), 'genSingle.pdf')
graph('abl', 'lower right', '-', (0, 1), 'genAbl.pdf')

