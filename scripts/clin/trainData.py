import sys
import matplotlib.pyplot as plt

def getData(inFile):
    data = []
    
    for line in open(inFile):
        line=line.strip()
        score = float(line[line.rfind('0.'):])
        trainSize = line[:line.find(':')]
        trainSize = int(trainSize[trainSize.rfind('.')+1:])
        data.append((trainSize, score))
    
    x = []
    y = []
    for instance in sorted(data, key=lambda tup: tup[0]):
        x.append(instance[0])
        y.append(instance[1])
    print(x)
    print(y)
    print()
    return ([x,y])
    

line1 = getData(sys.argv[1])
line2 = getData(sys.argv[2])

plt.plot(line1[0], line1[1], label='LiLiu', linestyle='--', linewidth=3)
plt.plot(line2[0], line2[1], label='LexNorm2015', linewidth=3, color='r')
plt.xlabel('Number of Tweets used for training')
plt.ylabel('F1')
plt.legend(loc='lower right')
#plt.ylim(rang)
plt.savefig('trainData.pdf', bbox_inches='tight')

