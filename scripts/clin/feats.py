import sys
import matplotlib.pyplot as plt

data = [0] * 11
names = ['all', 'w2v', 'asp', 'lookup', 'word.*', 'split', 'ngram', 'dict', 'char order', 'length', 'contAlpha']
for i in range(len(names)):
    names[i] = '-' + names[i]
names[0] = 'all'
for line in open(sys.argv[1]):
    line=line.strip()
    score = float(line.split()[-1])
    feats = line[:line.find(':')]
    feats = feats[-10:]
    if feats.count('1') == 10:
        data[0] = score
    else:
        featIdx = feats.find('0') + 1
        data[featIdx] = score

plt.bar(range(len(data)), data,color='b', label='')
plt.xticks(range(len(data)), names, rotation=30, ha='center')
plt.xlim([0, len(data)])
print(data)
#plt.xlabel()
plt.ylabel('accuracy')
plt.ylim((.6,.9))
plt.savefig('chenliAbl.pdf', bbox_inches='tight')
plt.cla()
plt.clf()

data = [0] * 11

for line in open(sys.argv[2]):
    line=line.strip()
    score = float(line.split()[-1])
    feats = line[:line.find(':')]
    feats = feats[-10:]
    if feats.count('1') == 10:
        data[0] = score
    else:
        featIdx = feats.find('0') + 1
        data[featIdx] = score

plt.bar(range(len(data)), data,color='b', label='')
plt.xticks(range(len(data)), names, rotation=30, ha='center')
plt.xlim([0, len(data)])
print(data)
#plt.xlabel()
plt.ylabel('F1')
plt.ylim((.6,.9))
plt.savefig('lexnormAbl.pdf', bbox_inches='tight')



