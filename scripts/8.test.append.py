import myutils

def getPerf(lang):
    err= accBase= acc= accGoldErr= accOracle= prec= rec= F1= realPrec= realF1= WER= CER= EDprec= EDrec= EDf1= TP= FN= FP= FN = 0.0
    for line in open('preds/test.' + lang + '.out'):
        tok = line.split()
        if line.startswith("ERR"):
            err = float(tok[-1])
        if line.startswith("Baseline"):
            accBase = float(tok[-2])
        if line.startswith("    1"):
            acc = float(tok[-1])
        if line.startswith("Upperbound"):
            accOracle = float(tok[-2])
        if line.startswith("Normalization"):
            prec = float(tok[2])
            rec = float(tok[1])
            F1 = float(tok[-1])
        if line.startswith('Real Prec'):
            realPrec = float(tok[-1])
        if (realPrec + rec) != 0:
            realF1 = 2 * (realPrec * rec) / (realPrec + rec)
        if line.startswith('WER'):
            WER = float(tok[-1])
        if line.startswith('CER'):
            CER = float(tok[-1])
        if line.startswith('Error Det'):
            EDprec = float(tok[3])
            EDrec = float(tok[2])
            EDf1 = float(tok[-1])
        if line.startswith('TP'):
            TP = int(tok[-1])
        if line.startswith('TN'):
            TN = int(tok[-1])
        if line.startswith('FP'):
            FP = int(tok[-1])
        if line.startswith('FN'):
            FN = int(tok[-1])
    for line in open('preds/test.' + lang + '.gold.out'):
        if line.startswith("    1"):
            accGoldErr = float(line.split()[-1])
            accGoldErr = accBase + (accGoldErr * (1-accBase))
    return [err, accBase, acc, accGoldErr, accOracle, prec, rec, F1, realPrec, realF1, WER, CER, EDprec, EDrec, EDf1, TP, FN, FP, FN]


def norm (score):
    if type(score) == int:
        return str(score).ljust(7)
    if type(score) == float:
        if score >= 0 and score <= 1:
            return str('{0:.2f}'.format(score*100)).ljust(7)
    if type(score) == str:
        return score.ljust(7)
metrics = ['ERR', 'accBase', 'accuracy', 'accGoldErr', 'accOracle', 'precision', 'recall', 'F1', 'realPrec', 'realF1', 'WER', 'CER', 'EDprec', 'EDrec', 'EDf1', 'TP', 'FN', 'FP', 'FN']

print('\small')
print('\\begin{tabular}{l | r r r r r r r}')
print('     \\toprule')
print('               ', end='')
for langIdx, lang in enumerate(myutils.langs):
    print (' & ' + myutils.corpora[langIdx].ljust(11), end='')
print('\\\\')
print('    \\midrule')
print('    Language   ', end='')
for language in ['NL', 'ES', 'EN', 'EN', 'SL', 'HR', 'SR']:
    print(' & ' + language.ljust(11), end='')
print('\\\\')

print('    \\midrule')

data = []
for langIdx, lang in enumerate(myutils.langs):
    data.append([0.0] * len(metrics))
    for metricIdx, score in enumerate(getPerf(lang)):
        data[langIdx][metricIdx] = score

for metricIdx in range(len(metrics)):
    print ('    ' + metrics[metricIdx].ljust(11), end = '')
    for langIdx, lang in enumerate(myutils.langs):
        print(' & ' + norm(data[langIdx][metricIdx]).ljust(11),  end='')
    print(' \\\\')


    #print('         ' + myutils.corpora[langIdx].ljust(13), end=' & ')
    #print(' & '.join([str(round(x*100,2)).ljust(5,'0') for x in getPerf(lang)]) + ' \\\\')
    
print('    \\bottomrule')
print('\\end{tabular}')


