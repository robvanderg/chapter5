import myutils

for lang in myutils.langs:
    for weight in range(0,21):
        weight = weight * 0.05
        weightStr = str(round(weight,3))
        out = '../preds/errDet.' + lang + '.' + weightStr
        cmd = './tmp/bin/binary -m TR -i ../data/' + lang + '/train -r ../working/' + lang + '.' + weightStr + ' -d ../data/' + lang + ' -D ../data/' + lang + '/dev -e ' + weightStr + ' > ' + out + '.out 2> ' + out + '.err'
        print (cmd)
    out = '../preds/errDet.' + lang + '.voc'
    cmd = './tmp/bin/binary -m TR  -i ../data/' + lang + '/train -r ../working/voc.' + lang + ' -d ../data/' + lang + ' -D ../data/' + lang + '/dev -u ../data/' + lang + '/aspell > ' + out + '.out 2> ' + out + '.err'
    print(cmd)

