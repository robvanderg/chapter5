mkdir data
#en1
mkdir data/en1
wget https://github.com/noisy-text/noisy-text.github.io/raw/master/2015/files/lexnorm2015.tgz
tar -zxvf lexnorm2015.tgz
python3 utils/json2norm.py lexnorm2015/test_truth.json > data/en1/test
python3 utils/json2norm.py lexnorm2015/train_data.json > lexnorm2015/train
python3 utils/split.py lexnorm2015/train 0 2360 > data/en1/train
python3 utils/split.py lexnorm2015/train 2360 9999 > data/en1/dev
cat data/en1/train data/en1/dev > data/en1/traindev
rm -rf lexnorm2015 lexnorm2015.tgz

cd data/en1
ln -s ../en/aspell aspell
ln -s ../en/aspell-model aspell-model
ln -s ../en/twitter.ngr.bin twitter.ngr.bin
ln -s ../en/w2v.bin w2v.bin
ln -s ../en/w2v.bin.cache w2v.bin.cache
ln -s ../en/wiki.ngr.bin wiki.ngr.bin
cd ../../

#en2
mkdir data/en2
wget http://people.eng.unimelb.edu.au/tbaldwin/etc/lexnorm_v1.2.tgz
tar -zxvf lexnorm_v1.2.tgz
mv data/corpus.v1.2.tweet data/en2/test
rm data/README.txt

wget http://www.hlt.utdallas.edu/~chenli/normalization/2577_tweets
python3 utils/split.py 2577_tweets 0 2060 > data/en2/train
python3 utils/split.py 2577_tweets 2060 9999 > data/en2/dev
cat data/en2/train data/en2/dev > data/en2/traindev

cd data/en2
ln -s ../en/aspell aspell
ln -s ../en/aspell-model aspell-model
ln -s ../en/twitter.ngr.bin twitter.ngr.bin
ln -s ../en/w2v.bin w2v.bin
ln -s ../en/w2v.bin.cache w2v.bin.cache
ln -s ../en/wiki.ngr.bin wiki.ngr.bin
cd ../../

rm lexnorm_v1.2.tgz 2577_tweets

#es
mkdir data/es
wget http://komunitatea.elhuyar.org/tweet-norm/files/2019/01/tweet-norm_esV3.zip
unzip tweet-norm_esV3.zip
python3 utils/esNorm.py tweet-norm_esV3/tweet-norm-dev500_annotated.txt tweet-norm_esV3/dev1
python3 utils/esNorm.py tweet-norm_esV3/tweet-norm-dev100_annotated.txt tweet-norm_esV3/dev2
cat tweet-norm_esV3/dev1 tweet-norm_esV3/dev2 > data/es/traindev
python3 utils/split.py data/es/traindev 0 454 > data/es/train
python3 utils/split.py data/es/traindev 454 9999 > data/es/dev
python3 utils/esNorm.py tweet-norm_esV3/tweet-norm-test_annotated.txt data/es/test
rm -rf tweet-norm_esV3 tweet-norm_esV3.zip

#sr
mkdir data/sr
wget https://www.clarin.si/repository/xmlui/bitstream/handle/11356/1171/ReLDI-sr.vert.zip
unzip ReLDI-sr.vert.zip
python3 utils/xml2norm.py ReLDI-sr.vert/reldi_sr.vert > sr

python3 utils/split.py sr 0 4138 > data/sr/train
python3 utils/split.py sr 4138 5518 > data/sr/dev
python3 utils/split.py sr 0 5518 > data/sr/traindev
python3 utils/split.py sr 5518 9999 > data/sr/test
rm -rf ReLDI-sr.vert.zip sr ReLDI-sr.vert

#hr
mkdir data/hr
wget https://www.clarin.si/repository/xmlui/bitstream/handle/11356/1170/ReLDI-hr.vert.zip
unzip ReLDI-hr.vert.zip
python3 utils/xml2norm.py ReLDI-hr.vert/reldi_hr.vert > hr

python3 utils/split.py hr 0 4762 > data/hr/train
python3 utils/split.py hr 4762 6350 > data/hr/dev
python3 utils/split.py hr 0 6350 > data/hr/traindev
python3 utils/split.py hr 6350 9999 > data/hr/test
rm -rf ReLDI-hr.vert.zip hr ReLDI-hr.vert

#sl
mkdir data/sl
wget https://www.clarin.si/repository/xmlui/bitstream/handle/11356/1123/Janes-Tag.vert.zip
unzip Janes-Tag.vert.zip
python3 utils/xml2norm.py janes.tag.vert > sl
python3 utils/split.py sl 0 4670 > data/sl/train
python3 utils/split.py sl 4670 6227 > data/sl/dev
python3 utils/split.py sl 0 6227 > data/sl/traindev
python3 utils/split.py sl 6227 9999 > data/sl/test

python3 utils/slSplit.py janes.tag.vert
python3 utils/xml2norm.py l1.norm.xml > l1.norm
python3 utils/xml2norm.py l3.norm.xml > l3.norm
python3 utils/split.py l1.norm 0 909 > data/sl/tweet.L1.traindev
python3 utils/split.py l1.norm 909 9999 > data/sl/tweet.L1.test
python3 utils/split.py l3.norm 0 5317 > data/sl/tweet.L3.traindev
python3 utils/split.py l3.norm 5317 9999 > data/sl/tweet.L3.test

rm -rf Janes-Tag.vert.zip janes.tag.* sl l[13].norm l[13].norm.xml

#nl
mkdir data/nl
cp ~/Downloads/ghentNorm nl
python3 utils/split.py nl 0 577 > data/nl/train
python3 utils/split.py nl 577 769 > data/nl/dev
python3 utils/split.py nl 0 769 > data/nl/traindev
python3 utils/split.py nl 769 9999 > data/nl/test
rm nl
