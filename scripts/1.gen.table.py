import myutils

def getNumCands(path):
    for line in open(path):
        if line.startswith('Average candidates:'):
            return float(line.split()[-1])
    return 0.0


print('\\begin{table}')
print('    \\centering')
print('    \\begin{tabular} {l r}')
print('         \\toprule')
print('         Module        & Avg. Candidates \\\\')
print('         \\midrule')
for featIdx, feat in enumerate(myutils.featNames[:5]):
    totalCands = 0
    featStr = ['0'] * 11
    featStr[featIdx] = '1'
    featStr = ''.join(featStr)
    for langIdx, lang in enumerate(myutils.langs):
        totalCands += getNumCands('.'.join(['preds/gen', lang, featStr, 'out'])) 
    print('         ' + myutils.featNames[featIdx].ljust(12) + ' & ' + '{:.3}'.format(totalCands/len(myutils.langs))+ '\\\\')
print('         \\bottomrule')
print('    \\end{tabular}')
print('    \\caption{The average number of candidates per word for each module, averaged over all corpora.}')
print('    \\label{tab:numNormCands}')
print('\\end{table}')

