# If more than 5 arguments are given, it will run using slurm
function run {
    if [ "$6" ];
    then
        python3 ../scripts/pg.prep.py $1 $2 $3 $4 $5
        python3 ../scripts/pg.run.py $2.[0-9]*
    else
        chmod +x $1
        bash $1
    fi
}

# ESPECIALLY THE SL DATA (AND SR) NEEDS A LOT OF MEMORY, FOR THE OTHERS 36GB SHOULD BE ENOUGH
#./scripts/0.prepData.sh
#./scripts/0.getNormData.sh

./scripts/1.gen.run.sh > src/1.gen.run.sh
cd src 
run 1.gen.run.sh 1.gen 2 50 4 $1
cd ..

./scripts/2.rank.run.sh > src/2.rank.run.sh
cd src 
run 2.rank.run.sh 2.rank 3 80 4 $1
cd ..

python3 ./scripts/3.learningC.run.py > src/3.learningC.run.sh
cd src 
run 3.learningC.run.sh 3.learningC 3 80 4 $1
cd ..

#python3 scripts/6.tuneW.run.py > src/6.tuneW.run.sh
#run 1.gen.run.sh 1.gen 1 50 4 $1

python3 ./scripts/7.errDet.run.py > src/7.errDet.run.sh
cd src 
run 7.errDet.run.sh 7.errDet 3 80 4 $1
cd ..

python3 ./scripts/8.test.run.py > src/8.test.run.sh
cd src 
run 8.test.run.sh 8.test 3 110 4 $1
cd ..

