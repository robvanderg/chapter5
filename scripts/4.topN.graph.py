import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

if len(sys.argv) < 3:
    print("usage:")
    print("grep \"^ *[0-9]\" preds/rank.*11111111111* > results")
    print("grep \"^Upperbound\" preds/rank.*11111111111* > results2")
    print("python3 scripts/4.topN.graph.py results results2 ")
    exit(1)

data ={}
fig, ax = plt.subplots(figsize=(8,5), dpi=300)

for line in open(sys.argv[1]):
    tok = line.split('/')[-1].split()
    lang = tok[0].split('.')[1]
    topN = int(tok[1])
    acc =  float(tok[-1])

    if lang not in data:
        data[lang] = {}
    data[lang][topN] = acc

upperbounds = {}
for line in open(sys.argv[2]):
    tok = line.split('/')[-1].split()
    lang = tok[0].split('.')[1]
    upperbounds[lang] = float(tok[-2])
    print(tok[-2])

for langIdx, lang in enumerate(myutils.langs):
    x = []
    y = []
    for topN in sorted(data[lang])[:-1]:
        x.append(topN)
        y.append(data[lang][topN])
    y.append(upperbounds[lang])
    x.append(x[-1] + 1)
    print(lang)
    print(x)
    print(y)
    mark = 's'
    if langIdx > 3:
        mark = 'X'

    ax.plot(x, y, linestyle='--', marker=mark, label=myutils.corpora[langIdx], linewidth=1.5, color=myutils.colors[langIdx])

    #y = [y[-1], upperbounds[lang]]
    #print(y)
    #ax.plot([9,10], y, linestyle='--', linewidth=1.5, color=myutils.colors[langIdx])

leg = ax.legend()
leg.get_frame().set_linewidth(1.5)
ax.set_ylabel('Recall')
ax.set_xlabel('N candidates')

x = []
labels = []
for i in range(1,10):
    x.append(i)
    labels.append(str(i))
x.append(10)
labels.append('upperbound')
plt.xticks(x, labels)

leg = ax.legend(loc='lower right')
leg.get_frame().set_linewidth(1.5)
fig.savefig('topn.pdf', bbox_inches='tight')
#plt.show()

